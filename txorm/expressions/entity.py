from zope.interface import implementer
from txorm.interfaces.model import IModel
from txorm.interfaces.field import IField
from txorm.interfaces.compiler import ICompilable
from txorm.expressions.util import compile_value
from txorm.expressions.operator import Node


__all__ = ['Table', 'Procedure', 'E']


@implementer(ICompilable)
class Table(object):
    def __init__(self, table):
        self.table = table

    def compile(self, compiler):
        if ICompilable.providedBy(self.table):
            return compile_value(self.table, compiler)
        if IModel.implementedBy(self.table):
            table_name = self.table.get_meta().get_tablename()
        else:
            table_name = self.table
        return '"{0}"'.format(table_name), None


@implementer(ICompilable)
class Procedure(object):
    procedure_name = ''

    def __init__(self, *params):
        self.params = params

    def get_params(self):
        return self.params

    def compile(self, compiler):
        params = self.get_params()
        ap = ', '.join([compiler.formatter] * len(params))
        sql = '{0}({1})'.format(self.procedure_name, ap)
        return sql, params


@implementer(ICompilable)
class Entity(Node):

    def __init__(self, entity):
        self.entity = entity

    def compile(self, compiler):
        if ICompilable.providedBy(self.entity):
            return self.entity.compile(compiler)
        if IField.providedBy(self.entity):
            return self.entity.get_sqlname(), None
        if IModel.implementedBy(self.entity):
            return '"{0}"'.format(self.entity.get_meta().get_tablename()), None
        return compiler.formatter, [self.entity]

E = Entity
