# -*-coding:utf-8-*-
from zope.interface import Interface


class IExpression(Interface):
    """
    Base node functionality
    """

    def equals(other):
        """
        Check if this node
        :param other:
        :return:
        """


__author__ = 'Vetal_krot'
