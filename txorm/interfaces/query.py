# -*-coding:utf-8-*-
from zope.interface import Interface
from txorm.interfaces.compiler import ICompilable


class IQueryContainer(Interface):
    """
    Container for query
    """


class IQueryBuilder(ICompilable):
    """
    Base interface for query builder
    """

    def set_backend(backend):
        """
        Set backend for this builder

        :param backend: `IBackend` implementer
        """

    def execute(backend):
        """
        Execute this query

        :return: defer.Deferred
        """

    def clone():
        """
        Make copy of this object
        """


class IInsertQuery(IQueryBuilder):
    """
    SQL select query interface
    """


class ISelectQuery(IQueryBuilder):
    """
    SQL select query interface
    """


class IUpdateQuery(IQueryBuilder):
    """
    SQL select query interface
    """


class IDeleteQuery(IQueryBuilder):
    """
    SQL select query interface
    """


__author__ = 'Vetal_krot'
