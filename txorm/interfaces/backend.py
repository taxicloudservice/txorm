# -*-coding:utf-8-*-
from zope.interface import Interface, Attribute


class IBackend(Interface):
    """
    Database backed.

    Provide executing queries
    """
    formatter = Attribute('db driver formatter (like %s or ?)')
    support_returning = Attribute("""Feature of db.
    Can return some after INSERT, UPDATE, DELETE""")
    support_for_update = Attribute("""Feature of db.
    DB can lock table to update rows""")

    def __init__(name, database, password, host, port, **options):
        """
        Init backend

        :param database: db name
        :param user: db user
        :param password: db password
        :param host: db host
        :param port: db port
        :param options: options specified for db driver
        """

    def db_insert(sql, params):
        """
        Execute sql insert query

        :param sql: query
        :type sql: str
        :param params: params for query
        :type params: list
        :return: last insert id
        :rtype: defer.Deferred
        """

    def db_select(sql, params):
        """
        Execute sql select query

        :param sql: query
        :type sql: str
        :param params: params for query
        :type params: list
        :return: result of query
        :rtype: defer.Deferred
        """

    def db_update(sql, params):
        """
        Execute sql update query

        :param sql: query
        :type sql: str
        :param params: params for query
        :type params: list
        :return: result of query
        :rtype: defer.Deferred
        """

    def db_delete(sql, params):
        """
        Execute sql delete query

        :param sql: query
        :type sql: str
        :param params: params for query
        :type params: list
        :return: result of query
        :rtype: defer.Deferred
        """

    def get_compiler():
        """
        Get sql compiler specific for using db

        :return: implementer of `ICompiler`
        :rtype: object
        """

    def get_expression(expression):
        """
        Get sql patter for given pattern

        :param expression: constant name of pattern
        :return: str or None
        """

    def start(self):
        """
        Start this backend. Will init the network connection or other

        :return: defer
        """

    def stop(self):
        """
        Stop this backend

        :return: defer
        """

__author__ = 'Vetal_krot'
