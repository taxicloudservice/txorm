# -*-coding:utf-8-*-
from zope.interface import Attribute
from txorm.interfaces.expression import IExpression


class IField(IExpression):
    abstract = Attribute('Field is abstract (virtual field)')

    def to_sql(value):
        """
        Convert python data to sql

        :param value: python data
        :return: sql data
        """

    def to_python(value):
        """
        Convert sql data to python object

        :param value: sql data
        :return: python object
        """

__author__ = 'Vetal_krot'
