# -*-coding:utf-8-*-
from txorm.models.fields.base import Field
from txorm.models.fields.validators import MinValueValidator, MaxValueValidator


__all__ = ['IntegerField', 'PositiveIntegerField', 'SmallIntegerField',
           'PositiveSmallIntegerField', 'BigIntegerField', 'FloatField']


class MinMaxIntegerField(Field):
    encoder = int
    max_value = None
    min_value = None

    def init_validators(self):
        Field.init_validators(self)
        if self.min_value is not None:
            self.validators.append(MinValueValidator(self.min_value))
        if self.max_value is not None:
            self.validators.append(MaxValueValidator(self.max_value))


class IntegerField(MinMaxIntegerField):

    def __init__(self, *args, **kwargs):
        self.min_value = kwargs.get('min_value')
        self.max_value = kwargs.get('max_value')
        Field.__init__(self, *args, **kwargs)

    def get_copy_kwargs(self):
        kwargs = MinMaxIntegerField.get_copy_kwargs(self)
        kwargs['min_value'] = self.min_value
        kwargs['max_value'] = self.max_value
        return kwargs


class PositiveIntegerField(IntegerField):
    min_value = 0


class SmallIntegerField(MinMaxIntegerField):
    max_value = 32767
    min_value = -max_value - 1


class PositiveSmallIntegerField(SmallIntegerField):
    min_value = 0


class BigIntegerField(MinMaxIntegerField):
    max_value = 9223372036854775807
    min_value = -max_value - 1


class FloatField(Field):
    encoder = float


__author__ = 'Vetal_krot'
