import inspect
from itertools import chain
from operator import attrgetter
from zope.interface import implementer
from twisted.internet import defer
from txorm import constants
from txorm.interfaces.relation import IRelatedField, IManyToManyField
from txorm.models.model import Registry
from txorm.models.fields.base import Field
from txorm.models.wrappers import ModelWrapper
from txorm.models.relation import (ManyToOneRelation, OneToManyRelation,
    OneToOneRelation, ManyToManyRelation)


__all__ = ['OneToOneField', 'ForeignKey', 'ManyToManyField']


class SetValueTriggerReceiver(object):
    def __init__(self, relation, value):
        self.relation = relation
        self.value = value
        self.related_field_name = relation.related_field.get_attrname()

    def receive(self, bundle):
        q = self.relation.get_related(bundle.instance).update()
        q.set(**{self.related_field_name: self.value})
        return q.execute()


class DeleteTriggerReceiver(object):
    def __init__(self, relation):
        self.relation = relation

    def receive(self, bundle):
        return self.relation.get_related(bundle.instance).delete().execute()


class RelationManager(object):
    def __init__(self, instance, relation):
        self.instance = instance
        self.relation = relation
        self._cache = None

    @property
    def cache(self):
        return self._cache

    @property
    def manager(self):
        q = self.relation.get_related(self.instance)
        q.callback_lazy(self.set_cache)
        return q

    def set_cache(self, res):
        self._cache = res
        return res

    def __nonzero__(self):
        return bool(self._cache)

    def __iter__(self):
        return iter(self._cache or ())


class RelationDescriptor(object):
    container_pattern = '_{0}_rel_manager'

    def __init__(self, relation, relation_manager, container_name):
        self.relation = relation
        self.relation_manager = relation_manager
        self.container_name = self.container_pattern.format(container_name)

    def extract_relation_manager(self, instance):
        relation_manager = getattr(instance, self.container_name, None)
        if relation_manager is None:
            relation_manager = self.relation_manager(instance, self.relation)
            setattr(instance, self.container_name, relation_manager)
        return relation_manager

    def __get__(self, instance, owner):
        if not instance:
            return self.relation
        return self.extract_relation_manager(instance)

    def __set__(self, instance, value):
        if instance:
            relation_manager = self.extract_relation_manager(instance)
            relation_manager.set_cache(value)


@implementer(IRelatedField)
class RelatedField(Field, RelationDescriptor):
    abstract = False
    self_relation = None
    reverse_relation = None
    relation_manager = RelationManager

    def __init__(self, related_model, to_field='pk', related_name=None,
                 symmetrical=True, on_delete=constants.CASCADE, *args, **kwargs):
        Field.__init__(self, *args, **kwargs)
        self.related_model = related_model
        self.to_field = to_field
        self.related_name = related_name
        self.relation = None
        self.symmetrical = symmetrical
        self.container_name = None
        self.on_delete = on_delete

    @property
    def related_field(self):
        return getattr(self.related_model, self.to_field)

    def init_validators(self):
        pass

    def get_copy_kwargs(self):
        kwargs = Field.get_copy_kwargs(self)
        kwargs['related_model'] = self.related_model
        kwargs['to_field'] = self.to_field
        kwargs['related_name'] = self.related_name
        kwargs['symmetrical'] = self.symmetrical
        kwargs['on_delete'] = self.on_delete
        return kwargs

    def set_attrname(self, name):
        Field.set_attrname(self, name)
        self.container_name = self.container_pattern.format(name)

    def _get_self_field(self):
        return self

    def _construct_reverse_relation(self):
        return self.reverse_relation(self.related_model, self.related_field,
                                     self.model, self._get_self_field())

    def _construct_self_relation(self):
        return self.self_relation(self.model, self._get_self_field(),
                                  self.related_model, self.related_field)

    def _init_triggers(self, self_relation, reverse_relation):
        pass

    def set_model(self, model):
        Field.set_model(self, model)
        if self.related_model is 'self':
            self.related_model = model.__name__
        if isinstance(self.related_model, (str, unicode)):
            model_path = self.model.__module__.split('.')
            model_path.append(None)
            rel_path = self.related_model.split('.')
            model_path[-len(rel_path):] = rel_path
            path = '.'.join(model_path)
            Registry().wait_model(path, self._lazy_construct_relations)
        else:
            self._construct_relations()

    def _lazy_construct_relations(self, related_model):
        self.related_model = related_model
        self._construct_relations()

    def _construct_relations(self):
        if self.related_model is self.model:
            self.related_model = ModelWrapper(self.related_model)
        self.relation = self._construct_self_relation()
        reverse_relation = self._construct_reverse_relation()
        self._init_triggers(self.relation, reverse_relation)
        if self.symmetrical:
            relation_name = self.related_name or self.model.get_meta().name + '_set'
            if hasattr(self.related_model, relation_name):
                raise ValueError(
                    "Related name '{0}' already exist for model '{1}'. "
                    "Set another `related_name` for field '{2}' "
                    "in model '{3}'".format(relation_name, self.related_model,
                                            self.get_attrname(), self.model))
            field_descriptor = RelationDescriptor(
                reverse_relation, self.relation_manager, relation_name)
            setattr(self.related_model, relation_name, field_descriptor)

    def to_sql(self, value):
        return getattr(value, self.related_field.get_attrname())


class WithValueFieldBase(RelatedField):

    def __init__(self, *args, **kwargs):
        RelatedField.__init__(self, *args, **kwargs)
        self.db_column_is_custom = 'db_column' in kwargs
        self._attrname = None
        self.value_field = None

    def _construct_relations(self):
        self.value_field = self.related_field.copy()
        self.value_field.null = self.null
        if self.db_column_is_custom:
            self.value_field.db_column = self.db_column
        else:
            self.value_field.db_column = None
        self.value_field.set_model(self.model)
        f_name = '{0}_{1}'.format(self.attrname, self.related_field.get_attrname())
        self.value_field.set_attrname(f_name)
        self.model.get_meta().fields[f_name] = self.value_field
        setattr(self.model, f_name, self.value_field)
        RelatedField._construct_relations(self)

    def _get_self_field(self):
        return self.value_field

    def _init_triggers(self, self_relation, reverse_relation):
        if self.on_delete is constants.CASCADE:
            trigger = DeleteTriggerReceiver(reverse_relation)
        elif self.on_delete is constants.SET_NULL:
            trigger = SetValueTriggerReceiver(reverse_relation, None)
        elif self.on_delete is constants.SET_DEFAULT and self.has_default():
            trigger = SetValueTriggerReceiver(reverse_relation,
                                              self.get_default())
        else:
            raise ValueError("Bad option on_delete for field {0} in model "
                             "{1}".format(self.attrname, self.model))
        self.related_model.triggers.before_delete.connect(trigger.receive)

    def __set__(self, instance, value):
        RelatedField.__set__(self, instance, value)
        if instance:
            if value is None:
                related_value = None
            else:
                related_value = getattr(value, self.to_field)
            setattr(instance, self.value_field.get_attrname(), related_value)


class OneToOneField(WithValueFieldBase):
    self_relation = reverse_relation = OneToOneRelation


class ForeignKey(WithValueFieldBase):
    self_relation = ManyToOneRelation
    reverse_relation = OneToManyRelation


class M2MRelationManager(RelationManager):
    def __init__(self, instance, relation, through_is_custom):
        RelationManager.__init__(self, instance, relation)
        self.through_is_custom = through_is_custom
        self.through_model = relation.through_model
        self.source_field_name = relation.source_field.get_attrname()
        self.target_field_name = relation.target_field.get_attrname()
        self._objects = set()
        self._new_objects = set()
        self._for_delete = set()

    def _permit_operation(self, name):
        if self.through_is_custom:
            raise AttributeError("Action {0} not allowed for M2M relation "
                                 "with custom through model".format(name))

    def add(self, *items):
        self._permit_operation('add')
        map(self._add_item, items)

    def _add_item(self, item):
        if inspect.isclass(item):
            raise ValueError("Item must be object. Class received.")
        if not item.pk:
            raise AttributeError("Model {0} must have pk".format(item))
        self._new_objects.add(item)

    def remove(self, *items):
        self._permit_operation('remove')
        map(self._remove_item, items)

    def _remove_item(self, item):
        if item in self._objects:
            self._objects.remove(item)
            self._for_delete.add(item)
        elif item in self._new_objects:
            self._new_objects.remove(item)
        else:
            raise ValueError("Item {0} not exist im model {1}".format(
                item, self.instance))

    def clear(self):
        self._permit_operation('clear')
        if self._objects:
            self._for_delete.update(self._objects)
            self._objects.clear()
        if self._new_objects:
            self._new_objects.clear()

    def save(self):
        return defer.DeferredList([self._do_insert(), self._do_delete()])

    def _do_insert(self):
        if not self._new_objects:
            return defer.succeed(None)
        through_models = []
        for item in self._new_objects:
            through_obj = self.through_model()
            setattr(through_obj, self.source_field_name, self.instance)
            setattr(through_obj, self.target_field_name, item)
            through_models.append(through_obj)
        d = self.through_model.manager.insert(*through_models).execute()
        d.addCallback(self._m2m_save_complete, self._new_objects.copy())
        return d

    def _m2m_save_complete(self, res, new_objects):
        self._objects.update(new_objects)
        self._new_objects -= new_objects
        return self

    def _do_delete(self):
        if not self._for_delete:
            return defer.succeed(None)
        through_source_field = self.relation.source_field.value_field
        to_field_name = self.relation.field.get_attrname()
        rel_values = map(attrgetter(to_field_name), self._for_delete)
        if len(rel_values) is 1:
            expr = through_source_field.eq(rel_values[0])
        else:
            expr = through_source_field.in_list(rel_values)
        q = self.through_model.manager.delete()
        d = q.filter(expr).execute()
        d.addCallback(self._m2m_delete_complete, self._for_delete.copy())
        return d

    def _m2m_delete_complete(self, res, del_objects):
        self._for_delete -= del_objects
        return self

    @property
    def cache(self):
        return self

    def set_cache(self, res):
        self._new_objects.clear()
        self._objects.clear()
        self._for_delete.clear()
        if isinstance(res, list):
            self._objects.update(res)
        else:
            self._objects.add(res)
        return res

    def __contains__(self, item):
        return item in self._new_objects or item in self._objects

    def __iter__(self):
        return chain(self._objects, self._new_objects)

    def __getitem__(self, item):
        return tuple(self)[item]

    def __str__(self):
        return str(list(self))

    def __nonzero__(self):
        return any((self._objects, self._new_objects))


def create_through_model(from_model, from_field, to_model, to_field, db_table):
    from txorm.models.model import Model

    def fk_db_column(model, field):
        return '{0}_{1}'.format(model.get_meta().name, field.db_column)

    from_name = from_model.get_meta().name
    to_name = to_model.get_meta().name
    through_model_name = '{0}To{1}ThroughModel'.format(from_name.capitalize(),
                                                       to_name.capitalize())
    from_db_column = fk_db_column(from_model, from_field)
    to_db_column = fk_db_column(to_model, to_field)
    if from_name is to_name:
        _from = 'from_{0}'
        _to = 'to_{0}'
        from_name = _from.format(from_name)
        to_name = _to.format(to_name)
        from_db_column = _from.format(from_db_column)
        to_db_column = _to.format(to_db_column)
    through_model = type(through_model_name, (Model,), {
        'Meta': type('Meta', (object,), {'db_table': db_table}),
        '__module__': from_model.__module__,
        from_name: ForeignKey(from_model, to_field=from_field.get_attrname(),
                              db_column=from_db_column, symmetrical=False),
        to_name: ForeignKey(to_model, to_field=to_field.get_attrname(),
                            db_column=to_db_column, symmetrical=False),
    })
    return through_model, (from_name, to_name)


class M2MRelationManagerBuilder(object):
    def __init__(self, relation_manager, through_is_custom):
        self.relation_manager = relation_manager
        self.through_is_custom = through_is_custom

    def __call__(self, instance, relation):
        return self.relation_manager(instance, relation, self.through_is_custom)


@implementer(IManyToManyField)
class ManyToManyField(RelatedField):
    abstract = True
    self_relation = reverse_relation = ManyToManyRelation

    def __init__(self, related_model, related_name=None, symmetrical=True,
                 through=None, to_field=('pk', 'pk'), through_fields=(),
                 through_db_table=None, **kwargs):
        RelatedField.__init__(self, related_model, to_field=to_field,
                              related_name=related_name,
                              symmetrical=symmetrical, **kwargs)
        self.from_field_name, self.to_field_name = to_field
        self._through = through
        self.through_fields = through_fields
        self._through_db_table = through_db_table
        self.through_is_custom = bool(through)
        self.relation_manager = M2MRelationManagerBuilder(
            M2MRelationManager, self.through_is_custom)

    def get_copy_kwargs(self):
        kwargs = RelatedField.get_copy_kwargs(self)
        kwargs['through_db_table'] = self._through_db_table
        if self.through_is_custom:
            kwargs['through'] = self._through
            kwargs['through_fields'] = self.through_fields
        return kwargs

    def _get_self_field(self):
        return getattr(self.model, self.from_field_name)

    @property
    def related_field(self):
        return getattr(self.related_model, self.to_field_name)

    @property
    def through_db_table(self):
        if self._through_db_table:
            return self._through_db_table
        return '{0}_{1}'.format(self.model.get_meta().db_table, self.db_column)

    def get_through_model(self):
        if not self._through:
            self._through, self.through_fields = create_through_model(
                self.model, getattr(self.model, self.from_field_name),
                self.related_model, self.related_field, self.through_db_table)
        return self._through

    def get_through_fields(self):
        through_model = self.get_through_model()
        source = through_model.__dict__[self.through_fields[0]]
        target = through_model.__dict__[self.through_fields[1]]
        return source, target

    def _construct_self_relation(self):
        through_model = self.get_through_model()
        source_field, target_field = self.get_through_fields()
        return self.self_relation(self.model, self._get_self_field(),
                                  self.related_model, self.related_field,
                                  through_model, source_field, target_field)

    def _construct_reverse_relation(self):
        through_model = self.get_through_model()
        target_field, source_field = self.get_through_fields()
        return self.reverse_relation(self.related_model, self.related_field,
                                     self.model, self._get_self_field(),
                                     through_model, source_field, target_field)
