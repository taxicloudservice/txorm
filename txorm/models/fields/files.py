# -*-coding:utf-8-*-
from txorm.models.fields.base import Field


__all__ = ['ImageField', 'FileField', 'FilePathField']


class FilePathField(Field):

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 100
        Field.__init__(self, *args, **kwargs)


ImageField = FileField = FilePathField