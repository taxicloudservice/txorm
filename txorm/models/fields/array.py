# -*-coding:utf-8-*-
from txorm.expressions.array import ArrayNode
from txorm.expressions.operator import Is, IsNot, Expression
from txorm.models.fields.base import BaseField
from txorm.models.fields.validators import ValidatorBase, ValidationError


__all__ = ['IntegerArrayField', 'FloatArrayField', 'CharArrayField', 'TextArrayField']


class ArrayFieldValidator(ValidatorBase):

    def validate(self, value):
        if not (isinstance(value, (list, tuple, set)) or value is None):
            raise ValidationError("Value for array field must be iterable or None")


class BaseArrayField(BaseField, ArrayNode):
    mutable = True
    _encoder = NotImplemented

    def __init__(self, *args, **kwargs):
        super(BaseArrayField, self).__init__(*args, **kwargs)
        self.validators.append(ArrayFieldValidator())

    def encoder(self, value):
        return map(self._encoder, value)

    def isnull(self, isnull):
        if isnull:
            return Is(self, None)
        return IsNot(self, None)


class IntegerArrayField(BaseArrayField):
    _encoder = int


class FloatArrayField(BaseArrayField):
    _encoder = float


class CharArrayField(BaseArrayField):
    _encoder = unicode
 
    def contains(self, other):
        return Contains(self, other)
 
 
class Contains(Expression):
    default_pattern = '{0} @> ARRAY[{1}]::varchar[]'


class TextArrayField(CharArrayField):
    pass