# -*-coding:utf-8-*-
import json
from txorm.models.fields.strings import TextField


__all__ = ['JSONField']


class JSONField(TextField):
    mutable = True

    def encoder(self, value):
        return json.dumps(value)

    def decoder(self, value):
        return json.loads(value) if value else {}