# -*-coding:utf-8-*-
from txorm.models.fields.base import Field


__all__ = ['BooleanField']


class BooleanField(Field):
    encoder = bool