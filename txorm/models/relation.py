# -*-coding:utf-8-*-
from zope.interface import implementer
from txorm.expressions import Join, LeftJoin, JoinExpressions
from txorm.interfaces.relation import (IOnoToOnoRelation, IOnoToManyRelation,
    IManyToOneRelation, IManyToManyRelation)


class RelationBase(object):

    def __init__(self, model, field, related_model, related_field):
        self.model = model
        self.field = field
        self.related_model = related_model
        self.related_field = related_field

    def get_related_model(self):
        return self.related_model

    @property
    def join_type(self):
        return LeftJoin if self.related_field.null else Join

    def get_join(self, join_type=None):
        join = join_type or self.join_type
        return join(self.get_related_model(), self.related_field == self.field)

    def create_related_query(self, related_value):
        query = self.related_model.manager.select()
        return self.build_filter(query, related_value)

    def build_filter(self, query, related_value):
        return query.filter(self.related_field == related_value)

    def extract_related_value(self, instance):
        return getattr(instance, self.field.get_attrname())

    def get_related(self, instance):
        related_value = self.extract_related_value(instance)
        return self.create_related_query(related_value)


class SingleRelatedResultMixin(object):

    def build_filter(self, query, related_value):
        return RelationBase.build_filter(self, query, related_value).single()


@implementer(IOnoToOnoRelation)
class OneToOneRelation(SingleRelatedResultMixin, RelationBase):
    pass


@implementer(IOnoToManyRelation)
class OneToManyRelation(RelationBase):
    pass


@implementer(IManyToOneRelation)
class ManyToOneRelation(SingleRelatedResultMixin, RelationBase):
    pass


@implementer(IManyToManyRelation)
class ManyToManyRelation(RelationBase):

    def __init__(self, model, field, related_model, related_field,
                 through_model, source_field, target_field):
        RelationBase.__init__(self, model, field, related_model, related_field)
        self.through_model = through_model
        self.source_field = source_field
        self.target_field = target_field
        # Relation from source model to through model
        self.source_through_relation = OneToManyRelation(
            self.model, self.field, self.through_model,
            self.source_field.value_field)
        # Relation from target model to through model
        self.target_through_relation = OneToManyRelation(
            self.related_model, self.related_field, self.through_model,
            self.target_field.value_field)
        # Relation from through model to target model
        self.through_target_relation = self.target_field.relation

    def get_join(self, join_type=None):
        join = join_type or self.join_type
        source_through_join = self.source_through_relation.get_join(join)
        through_target_join = self.through_target_relation.get_join(join)
        return JoinExpressions((source_through_join, through_target_join))

    def build_filter(self, query, related_value):
        query._join_relation(self.target_through_relation)
        query.filter(self.source_field.value_field == related_value)
        return query


__author__ = 'Vetal_krot'
