from twisted.trial import unittest
from txorm import models
from txorm.models.relation import (OneToOneRelation, OneToOneRelation,
    ManyToOneRelation, ManyToManyRelation)


class Model1(models.Model):
    class Meta:
        db_table = 'table1'

    field1 = models.IntegerField()


class Model2(models.Model):
    class Meta:
        db_table = 'table2'

    field2 = models.IntegerField()


class OnoToOnoRelationTest(unittest.TestCase):

    def test_relation(self):
        relation = OneToOneRelation(Model1, Model1.field1, Model2, Model1.pk)
        print relation.get_join()
        Model1.rel = relation
        print Model1(field1=2).rel