# -*-coding:utf-8-*-
from twisted.internet import defer
from twisted.trial import unittest
from txorm import models
from txorm.backends.postgresql.compiler import PGCompiler
from txorm.expressions import Procedure, MAX, COUNT
from txorm.router import SimpleRouter, register_router


class MyProcedure(Procedure):
    procedure_name = 'hard_func'


class Model1(models.Model):
    class Meta:
        db_table = 'model1'
        ordering = ['id', 'field2']

    field1 = models.CharField(max_length=10)
    field2 = models.IntegerField(null=True)


class Model2(models.Model):
    class Meta:
        db_table = 'model2'

    field1 = models.IntegerField()
    model1 = models.ForeignKey(Model1, related_name='model2', on_delete=models.CASCADE, null=True)


class Model3(models.Model):
    class Meta:
        db_table = 'tariffs'

    field1 = models.IntegerField(default=None)
    car_types = models.ManyToManyField(Model2)


class FakeBackend(object):
    compiler = PGCompiler()
    support_returning = True

    def db_select(self, sql, params):
        sql = sql.replace('%s', '{}')
        print '\r\nexecuting...', sql.format(*params)
        return defer.succeed([(10,)])

    db_insert = db_update = db_delete = db_select

    def get_compiler(self):
        return self.compiler

register_router(SimpleRouter(FakeBackend()))


class RelatedManagerTest(unittest.TestCase):

    def test_initmodels(self):
        q = Model1.manager.select().filter(field1__ne=MyProcedure(1, 3))
        q.filter(model2__pk__in_list=Model2.manager.select('id'),
                 model2__model3_set__field1__not_between=(u'asdfasd', u'ggggg'))
        q.order_by('-model2__model3_set')
        print q.count()
        m1 = Model1.load((3, 'fff', 2))
        m1.model2.execute()
        #
        # # q2 = Model2.manager.select().order_by(MyProcedure(1, 3)).all()
        m2 = Model2.load(('qqqq', 2, 10))
        m2.field1 = 9
        print m2._changed_fields
        m2.save()
        print m2._changed_fields
        # m2.delete()
        qm2 = m2.model1
        qm2.execute()

        m3 = Model1()
        m3.field1 = u'gggg'
        print 'Pre save', m3.pk
        m3.save()
        # print 'Post save', m3.pk
        # m3.field2 = 23423
        # m3.save()
        # print 'Post update', m3.pk
        m3.delete()
        # print 'Post delete', m3.pk
        m2 = Model2()
        m2.model1 = m3
        # Model1.manager.select().filter(field1__in_list=range(10))

        q5 = Model3.manager.select().filter(car_types__model1__field1__not_equals='!!!')
        q5.filter(car_types__pk=2)
        q5.execute()

        qq = m2.model3_set.filter(field1__between=[1, 2])
        qq.execute()


        m5 = Model1()
        # print Model1.get_meta().iterfields(True)
        m5.field1 = u'ssss'
        # m5.field2 = 123
        mm = Model2()
        mm.pk = 100
        m9 = Model3()
        m9.field1 = 3
        m9.car_types.add(Model2(id=100500), Model2(id=90))
        m9.save()
        print m9.car_types
        m9.delete()
        # # print m9.car_types.execute()
        # # m5.save()
        # q = Model1.manager.update().set(field1__add=2).filter(field1__in_list=range(10))
        # q.execute()
        m1 = Model1(id=12, field1='ffff', field2=2)
        m2 = Model2()
        m2.field1 = 23423
        m2.model1 = m1
        m2.save()
        m1.delete()