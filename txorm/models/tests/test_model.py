# -*-coding:utf-8-*-
from twisted.trial import unittest
from txorm import models


class TestModel(models.Model):
    class Meta:
        db_table = 'test_table'
        ordering = ['id']

    field1 = models.IntegerField()
    field2 = models.CharField(max_length=30)
    field3 = models.FloatField(db_column='float_field')


class ModelTest(unittest.TestCase):
    def setUp(self):
        self.tablename = 'test_table'
        self.model = TestModel

    def test_meta(self):
        self.assertEqual(self.model._meta.db_table, self.tablename)
        self.assertEqual(self.model.field1, self.model._meta.fields['field1'])
        self.assertEqual(self.model.field2, self.model._meta.fields['field2'])
        self.assertTrue(self.model._meta.fields['id'])
        self.assertTrue(self.model._meta.pk_field)

    def test_fields(self):
        self.assertEqual(self.model.field1.get_attrname(), 'field1')
        self.assertEqual(self.model.field1.db_table, self.tablename)
        self.assertEqual(self.model.field2.get_attrname(), 'field2')
        self.assertEqual(self.model.field2.db_table, self.tablename)

    def test_set(self):
        model = self.model(5, '4444', 12)
        print model.pk, bool(model.pk)
        model.field1 = 1
        model.field2='2324'
        print model._changed_fields


__author__ = 'Vetal_krot'
