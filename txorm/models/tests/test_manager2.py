# -*-coding:utf-8-*-
from twisted.internet import defer
from twisted.trial import unittest
from txorm.router import SimpleRouter, register_router
from txorm.backends.postgresql import PostgresSQLBackend
from txorm import models


pg_backend = PostgresSQLBackend('mdriver', 'mdriver', 'admin019283', '91.234.211.132')
router = SimpleRouter(pg_backend)
register_router(router)


class Tariff(models.Model):
    class Meta:
        db_table = 'tariffs'
        ordering = ['id', 'name']

    name = models.CharField(max_length=80)
    min_price = models.FloatField(default=0.0)
    distance_in_min_price = models.FloatField(default=0.0)
    min_price_operation = models.SmallIntegerField(default=1)
    time_in_min_price = models.FloatField(default=0.0)
    distance_unit_price = models.FloatField(default=0.0)
    time_unit_price = models.FloatField(default=0.0)
    waiting_unit_price = models.FloatField(default=0.0)
    idle_speed_limit = models.IntegerField(default=20)


class TariffZone(models.Model):
    class Meta:
        db_table = 'tariff_zones'

    tariff = models.ForeignKey(Tariff, related_name='zones')
    distance_unit_price = models.FloatField(default=0)
    time_unit_price = models.FloatField(default=0)
    start_charge = models.FloatField(default=0)
    end_charge = models.FloatField(default=0)

    def __unicode__(self):
        return self.zone.name


class TariffExtra(models.Model):
    class Meta:
        db_table = 'extras'

    CHOICE_EXTRA = (
        (1, u'Детское кресло'),
        (2, u'Кондиционер'),
        (3, u'Не курить'),
        (5, u'Не курить'),
    )

    tariff = models.ForeignKey(Tariff, related_name='extras')
    extra = models.SmallIntegerField(choices=CHOICE_EXTRA, db_index=True)
    price = models.FloatField(default=0)
    percent = models.FloatField(default=0)


class Test(unittest.TestCase):

    @defer.inlineCallbacks
    def test_load_tariff(self):
        tariff = yield Tariff.manager.select().first()
        extras = yield tariff.extras.all()
        extra = TariffExtra()
        extra.tariff = tariff
        extra.extra = 3
        yield extra.save()
        print extra.pk
        extra.extra = 5
        yield extra.save()
        print extras
        zones = yield tariff.zones.all()
        zone = zones[0]
        zone.distance_unit_price += 22
        yield zone.save()
