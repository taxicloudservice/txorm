# -*-coding:utf-8-*-
import sys
from itertools import chain
from copy import copy
from operator import itemgetter
from txorm.interfaces.model import IModel

from zope.interface import implementer
from twisted.python import failure
from twisted.internet import defer

from txorm.interfaces.field import IField
from txorm.interfaces.relation import IRelation
from txorm.interfaces.query import IQueryBuilder
from txorm.interfaces.compiler import ICompilable
from txorm.constants import EXPR_DELIMITER
from txorm.query import QueryContainer
from txorm.expressions import ASC, DESC, Node
from txorm.expressions import RawExpression, COUNT, AS, AND, Join, LeftJoin


@implementer(IQueryBuilder)
class TransactionManager(object):

    def __init__(self, backend=None):
        self.backend = backend
        self._queries = []

    def set_backend(self, backend):
        self.backend = backend

    def clone(self):
        new = self.__class__(self.backend)
        new._queries = copy(self._queries)
        return new

    def add(self, *queries):
        self._queries.extend(queries)

    def execute(self, backend=None):
        backend = backend or self.backend
        compiler = backend.get_compiler()
        query_list = []
        for query in self._queries:
            sql, params = query.compile(compiler)
            query_list.append((sql, params, query.fetch_result))
        return backend.execute_transaction(query_list)


@implementer(IQueryBuilder)
class QueryManagerBase(Node):

    def __init__(self, backend, model):
        self.model = model
        self.backend = backend
        self.container = QueryContainer()
        self.container.set_table(model)
        self.fetch_result = False

    def set_backend(self, backend):
        self.backend = backend

    def _get_query_compiler(self, compiler):
        raise NotImplementedError()

    def _get_query_executor(self, backend):
        raise NotImplementedError()

    def compile(self, compiler):
        compiler = self._get_query_compiler(compiler)
        return compiler(self.container)

    def execute(self, backend=None):
        backend = backend or self.backend
        sql, params = self.compile(backend.get_compiler())
        executor = self._get_query_executor(backend)
        return executor(sql, params, self.fetch_result)

    def clone(self):
        new = self.__class__(self.backend, self.model)
        new.container = self.container.clone()
        return new

    copy = clone


class SimpleSelectableManagerBase(QueryManagerBase):

    def _get_expression_by_string(self, string_expr, value):
        pieces = string_expr.split(EXPR_DELIMITER, 1)
        expr = 'equals'
        parent = self.model
        for piece in pieces:
            attr = getattr(parent, piece)
            if IField.providedBy(attr):
                parent = attr
            else:
                expr = piece
        return getattr(parent, expr)(value)

    def block(self, *args, **kwargs):
        expr_list = list(args)
        for k, v in kwargs.iteritems():
            expr_list.append(self._get_expression_by_string(k, v))
        return AND(expr_list)

    Q = block

    def filter(self, *args, **kwargs):
        for expr in args:
            self.container.add_where(expr)
        for k, v in kwargs.iteritems():
            self.container.add_where(self._get_expression_by_string(k, v))
        return self

    def where(self, sql, *params):
        self.container.add_where(RawExpression(sql, params))
        return self

    def all(self):
        return self.execute()


class ReturningManagerMixin(object):

    _return_models = True

    def returning(self, *fields):
        if not self.backend.support_returning:
            raise AttributeError("Backend {0} not support returning".format(
                self.backend))
        fls = []
        self.fetch_result = True
        self._return_models = False
        for field in fields:
            if isinstance(field, str):
                field = getattr(self.model, field)
            elif IField.providedBy(field) or ICompilable.providedBy(field):
                pass
            else:
                raise ValueError("Model {0} has not field {1}".format(
                    self.model, field))
            fls.append(field)
        self.container.add_returning(fls)
        return self


class SelectManager(SimpleSelectableManagerBase):

    def __init__(self, backend, model, fields=None):
        SimpleSelectableManagerBase.__init__(self, backend, model)
        self.fetch_result = True
        self._available_models = [model]
        self._related_selects = {}
        self._load_to_model = not fields
        self._lazy_callbacks = []
        self._single = False
        self._set_fields(fields or self.model.get_meta().iterfields())

    def _set_fields(self, fields):
        select_fields = []
        for field in fields:
            if isinstance(field, str):
                field = self._get_field_by_string(field)
            select_fields.append(field)
        self.container.add_fields(select_fields)

    def _get_field_by_string(self, expression):
        pieces = expression.split(EXPR_DELIMITER)
        parent = self.model
        for piece in pieces:
            attr = getattr(parent, piece)
            if IRelation.providedBy(attr):
                parent = self._join_relation(attr)
            else:
                parent = attr
        if not IField.providedBy(parent):
            parent = parent.pk
        return parent

    F = _get_field_by_string

    def _get_expression_by_string(self, string_expr, value):
        pieces = string_expr.split(EXPR_DELIMITER)
        expr = 'equals'
        parent = self.model
        for piece in pieces:
            attr = getattr(parent, piece)
            if IRelation.providedBy(attr):
                if value is None or pieces[-1] == 'isnull' and value is True:
                    join_type = LeftJoin
                else:
                    join_type = Join
                parent = self._join_relation(attr, join_type)
            elif IField.providedBy(attr):
                parent = attr
            else:
                expr = piece
        return getattr(parent, expr)(value)

    def _join_relation(self, relation, join_type=None):
        related_model = relation.get_related_model()
        if related_model not in self._available_models:
            self._available_models.append(related_model)
            self.container.add_join(relation.get_join(join_type))
        return related_model

    def callback_lazy(self, callback, *args, **kwargs):
        self._lazy_callbacks.append((callback, args, kwargs))

    def join(self, model, on):
        self.container.add_join(Join(model, on))
        return self

    def limit(self, limit):
        self.container.set_limit(limit)
        return self

    def offset(self, offset):
        self.container.set_offset(offset)
        return self

    def select_related(self, name):
        relation = getattr(self.model, name)
        query = relation.related_model.manager.select()
        self._related_selects[name] = query
        return query

    def count(self):
        self.container.remove_fields(self.model.get_meta().fields.itervalues())
        self._set_fields((COUNT(self.model),))
        self._load_to_model = False
        self._single = True
        d = self.execute()
        d.addCallback(itemgetter(-1))
        return d

    def annotate(self, **kwargs):
        fields = []
        for k, v in kwargs.iteritems():
            fields.append(AS(v, k))
        self.container.add_fields(fields)
        return self

    def distinct(self, *args):
        fields = []
        for field in args:
            if isinstance(field, str):
                field = self._get_field_by_string(field)
            fields.append(field)
        self.container.add_distinct(fields)
        return self

    def order_by(self, *fields):
        expressions = []
        for f in fields:
            if isinstance(f, (str, unicode)):
                if f.startswith('-'):
                    expr = DESC
                    f = f[1:]
                else:
                    expr = ASC
                field = self._get_field_by_string(f)
                expressions.append(expr(field))
            elif IField.providedBy(f):
                expressions.append(ASC(f))
            elif ICompilable.providedBy(f):
                expressions.append(f)
            else:
                raise ValueError('Bad attribute for ordering {0}'.format(f))
        self.container.add_order_by(expressions)
        return self

    def group_by(self, *args):
        fields = []
        for field in args:
            if isinstance(field, (str, unicode)):
                field = self._get_field_by_string(field)
            elif IField.providedBy(field):
                pass
            else:
                raise ValueError('Bad attribute for function group by {0}'.format(field))
            fields.append(field)
        self.container.add_group_by(fields)
        return self

    def single(self):
        self._single = True
        self.limit(1)
        return self

    def first(self):
        self.single()
        self.order_by(self.model.pk)
        return self.execute()

    def last(self):
        self.single()
        self.order_by(DESC(self.model.pk))
        return self.execute()

    def get(self, *args, **kwargs):
        self.single()
        self.filter(*args, **kwargs)
        return self.execute()

    def __getitem__(self, item):
        self.single()
        if item >= 1:
            self.offset(item - 1)
        return self.execute()

    def __getslice__(self, offset, limit):
        if limit != sys.maxint:
            self.limit(limit - offset)
        if offset:
            self.offset(offset)
        return self.execute()

    def _apply_meta_ordering(self):
        if (self._load_to_model and not self.container._order_by and
                self.model.get_meta().ordering):
            self.order_by(*self.model.get_meta().ordering)

    def _get_query_compiler(self, compiler):
        return compiler.compile_select

    def _get_query_executor(self, backend):
        return backend.db_select

    def compile(self, compiler):
        self._apply_meta_ordering()
        return SimpleSelectableManagerBase.compile(self, compiler)

    def execute(self, backend=None, require_related=True):
        d = SimpleSelectableManagerBase.execute(self, backend)
        if self._load_to_model:
            d.addCallback(lambda res: map(self.model.load, res))
        if self._single:
            d.addCallback(lambda res: res[0] if res else failure.Failure(
                self.model.DoesNotExist()))
        if self._lazy_callbacks:
            for callback, args, kwargs in self._lazy_callbacks:
                d.addCallback(callback, *args, **kwargs)
        if self._related_selects:
            d.addCallback(self._load_related, require_related)
        return d

    def _load_related(self, res, require_related):
        models = res if isinstance(res, list) else (res,)
        defer_list = []
        for model in models:
            for rel_name, query in self._related_selects.iteritems():
                relation = getattr(self.model, rel_name)
                related_value = relation.extract_related_value(model)
                if related_value is None:
                    continue
                q = relation.build_filter(query.clone(), related_value)
                rel = getattr(model, rel_name)
                _d = q.execute(require_related=require_related)
                _d.addCallback(rel.set_cache)
                defer_list.append(_d)
        d = defer.DeferredList(defer_list, consumeErrors=not require_related,
                               fireOnOneErrback=require_related)
        d.addCallback(lambda a: res)
        return d

    def clone(self):
        new = SimpleSelectableManagerBase.clone(self)
        new._available_models = copy(self._available_models)
        new._related_selects = self._related_selects.copy()
        new._load_to_model = self._load_to_model
        new._single = self._single
        return new

    def delete(self):
        q = self.model.manager.delete()
        q.container = self.container.clone()
        q._available_models = copy(self._available_models)
        return q

    def update(self, **kwargs):
        q = self.model.manager.update()
        q.container = self.container.clone()
        q.set(**kwargs)
        return q


class InsertManagerMixin(object):

    def _values_dict(self, model):
        values = []
        for field in self.model.get_meta().iterfields(True):
            value = getattr(model, field.get_attrname(), None)
            if not (IField.providedBy(value) or ICompilable.providedBy(value)):
                value = field.to_sql(value)
            values.append(value)
        return values

    def _set_pk(self, res):
        for model, pk_value in zip(self.models, res):
            model.set_pk(pk_value[0])
        return self.models

    def _get_query_compiler(self, compiler):
        return compiler.compile_insert

    def _get_query_executor(self, backend):
        return backend.db_insert


class BulkInsertManager(InsertManagerMixin, QueryManagerBase,
                        ReturningManagerMixin):

    def __init__(self, backend, model, *models):
        QueryManagerBase.__init__(self, backend, model)
        self.fetch_result = True
        self.models = []
        fls = self.model.get_meta().iterfields(True)
        self.container.add_fields(f.get_sqlname(False) for f in fls)
        self.container.add_returning([self.model.pk.get_sqlname()])
        self.add(*models)

    def add(self, *models):
        values = map(self._values_dict, models)
        self.container.add_values(values)
        self.models.extend(models)
        return self

    def _returned(self, res):
        self._set_pk(res)
        return [v[1:] for v in res]

    def execute(self, backend=None):
        d = QueryManagerBase.execute(self, backend)
        if self._return_models:
            d.addCallback(self._set_pk)
        else:
            d.addCallback(self._returned)
        return d


class SingleInsertManager(InsertManagerMixin, QueryManagerBase):

    def __init__(self, backend, model, *models):
        QueryManagerBase.__init__(self, backend, model)
        self.fetch_result = True
        self.models = []
        fls = self.model.get_meta().iterfields(True)
        self.fields = [f.get_sqlname(False) for f in fls]
        self.add(*models)

    def add(self, *models):
        self.models.extend(models)
        return self

    def execute(self, backend=None):
        backend = backend or self.backend
        compiler = self._get_query_compiler(backend.get_compiler())
        executor = self._get_query_executor(backend)
        defer_list = []
        for model in self.models:
            container = QueryContainer()
            container.set_table(model.get_meta().db_table)
            container.add_fields(self.fields)
            container.add_values([self._values_dict(model)])
            sql, params = compiler(container)
            defer_list.append(executor(sql, params))
        d = defer.gatherResults(defer_list)
        d.addCallback(lambda res: chain(*res))
        d.addCallback(self._set_pk)
        return d


class UpdateManager(SimpleSelectableManagerBase, ReturningManagerMixin):

    def __init__(self, backend, model, **values):
        SimpleSelectableManagerBase.__init__(self, backend, model)
        if values:
            self.set(**values)

    def set(self, **kwargs):
        values = {}
        for str_expr, value in kwargs.iteritems():
            pieces = str_expr.split(EXPR_DELIMITER, 1)
            field = self.model.get_meta().fields[pieces[0]]
            if len(pieces) > 1:
                value = getattr(field, pieces[1])(value)
            elif IField.providedBy(value) or ICompilable.providedBy(value):
                pass
            else:
                value = field.to_sql(value)
            values[field.get_sqlname(False)] = value
        self.container.add_update(values)
        return self

    def _get_query_compiler(self, compiler):
        return compiler.compile_update

    def _get_query_executor(self, backend):
        return backend.db_update

    def for_update(self, nowait=False):
        self.query.for_update(nowait)
        return self


class DeleteManager(SimpleSelectableManagerBase, ReturningManagerMixin):

    def _get_query_compiler(self, compiler):
        return compiler.compile_delete

    def _get_query_executor(self, backend):
        return backend.db_delete


class QueryBuilder(object):

    def __init__(self, model, router):
        self.model = model
        self.router = router

    def select(self, *fields):
        backend = self.router.for_select(self.model)
        return SelectManager(backend, self.model, fields)

    def insert(self, *instances):
        backend = self.router.for_insert(self.model)
        if backend.support_returning:
            return BulkInsertManager(backend, self.model, *instances)
        return SingleInsertManager(backend, self.model, *instances)

    def update(self, **values):
        backend = self.router.for_update(self.model)
        return UpdateManager(backend, self.model, **values)

    def delete(self):
        backend = self.router.for_delete(self.model)
        return DeleteManager(backend, self.model)

    def transaction(self, backend=None):
        backend = backend or self.router.for_update(self.model)
        return TransactionManager(backend)

    @defer.inlineCallbacks
    def get_or_create(self, **kwargs):
        try:
            result = yield self.select().get(**kwargs)
            created = False
        except self.model.DoesNotExist:
            result = self.model(**kwargs)
            yield result.save()
            created = True
        defer.returnValue((result, created))

    def AS(self, name=None):
        from txorm.models import wrappers
        model = wrappers.ModelWrapper(self.model)
        return type(self)(model, self.router)

    def __getattr__(self, item):
        return getattr(self.select(), item)


class Manager(object):
    router = None

    def __get__(self, instance, owner):
        return QueryBuilder(owner, self.router)

    @classmethod
    def set_router(cls, router):
        cls.router = router


__author__ = 'Vetal_krot'
