# -*-coding:utf-8-*-
from collections import namedtuple
from zope.interface import implementer
from twisted.internet import defer
from txorm.interfaces.field import IField
from txorm.interfaces.model import IModelOptions, IModel
from txorm.interfaces.relation import IRelatedField, IManyToManyField, IRelation
from txorm.models.manager import Manager
from txorm.models.fields.digits import IntegerField
from txorm.models.fields.base import PrimaryKeyAbstractField


@implementer(IModelOptions)
class ModelOptions(object):
    def __init__(self, name, meta):
        self.name = name
        self.db_table = getattr(meta, 'db_table', name)
        self.ordering = getattr(meta, 'ordering', None)
        self.abstract = getattr(meta, 'abstract', False)
        self.m2m_fields = []
        self.foreign_keys = []
        self.fields = {}
        self.mutable_fields = []
        self.pk_field = None

    def get_tablename(self):
        return self.db_table

    def iterfields(self, exclude_pk=False):
        if exclude_pk:
            fields = self.fields.copy()
            del fields[self.pk_field.get_attrname()]
        else:
            fields = self.fields
        return fields.itervalues()


class Trigger(object):

    def __init__(self, trigger_name, bundle_fields):
        self.subscribers = []
        self.bundle_factory = namedtuple(trigger_name, bundle_fields)

    def connect(self, subscriber, *args, **kwargs):
        if not callable(subscriber):
            raise ValueError("Trigger subscriber must be callable")
        self.subscribers.append((subscriber, args, kwargs))

    def disconnect(self, subscriber):
        for s in self.subscribers:
            if subscriber == s[0]:
                self.subscribers.remove(s)
                break

    def send(self, *args, **kwargs):
        defer_list = []
        bundle = self.bundle_factory(*args, **kwargs)
        for subscriber, a, kw in self.subscribers:
            sd = defer.maybeDeferred(subscriber, bundle, *a, **kw)
            defer_list.append(sd)
        d = defer.DeferredList(defer_list)
        d.addCallback(lambda res: None)
        return d

    def __call__(self, *args, **kwargs):
        return self.send(*args, **kwargs)


class Registry(object):
    models = {}
    lazy_funcs = {}

    def wait_model(self, path, f):
        if path in self.models:
            f(self.models[path])
        else:
            self.lazy_funcs.setdefault(path, []).append(f)

    def add_model(self, model):
        path = '.'.join((model.__module__, model.__name__))
        self.models[path] = model
        lazy_funcs = self.lazy_funcs.pop(path, ())
        for f in lazy_funcs:
            f(model)


class ModelTriggers(object):

    def __init__(self):
        self.before_insert = Trigger('BEFORE_INSERT_TRIGGER',
                                     ('instance', 'model'))
        self.before_update = Trigger('BEFORE_UPDATE_TRIGGER',
                                     ('instance', 'model', 'changed_fields'))
        self.before_delete = Trigger('BEFORE_DELETE_TRIGGER',
                                     ('instance', 'model'))
        self.after_insert = Trigger('AFTER_INSERT_TRIGGER',
                                    ('instance', 'model'))
        self.after_update = Trigger('AFTER_UPDATE_TRIGGER',
                                    ('instance', 'model', 'changed_fields'))
        self.after_delete = Trigger('AFTER_DELETE_TRIGGER',
                                    ('instance', 'model'))


class ModelMetaclass(type):
    registry = Registry()

    def __new__(cls, name, bases, attrs):
        # Copy all fields from base abstract models
        model_attrs = {}

        def copy_fields(fields):
            return dict((f.get_attrname(), f.copy()) for f in fields)

        for base in bases:
            base_meta = getattr(base, '_meta', None)
            if not IModel.implementedBy(base):
                continue
            model_attrs.update(copy_fields(base_meta.iterfields()))
            model_attrs.update(copy_fields(base_meta.m2m_fields))
            model_attrs.update(copy_fields(base_meta.foreign_keys))

        model_attrs.update(attrs)
        model_meta = attrs.pop('Meta')
        model_attrs['_meta'] = ModelOptions(name.lower(), model_meta)
        model_attrs['DoesNotExist'] = type(
            'DoesNotExist', (Exception,),
            {'__module__': '{}.{}'.format(attrs['__module__'], name)})
        model_attrs['triggers'] = ModelTriggers()
        model = type.__new__(cls, name, bases, model_attrs)
        model.init_fields(model_attrs)
        model.init_pk()
        model.init_relations(model_attrs)
        cls.registry.add_model(model)
        return model

    def get_pk_filed(cls, fields):
        for field in fields.itervalues():
            if field.primary_key:
                return field

    def init_fields(cls, fields):
        for name, field in fields.iteritems():
            if IRelation.providedBy(field):
                continue
            if IField.providedBy(field):
                field.set_attrname(name)
                if not cls._meta.abstract:
                    field.set_model(cls)
                cls._meta.fields[name] = field
                if field.mutable:
                    cls._meta.mutable_fields.append(name)
                if field.primary_key and cls._meta.pk_field:
                    raise ValueError("Got multiple primary key "
                                     "for model {0}".format(cls))
                elif field.primary_key:
                    cls._meta.pk_field = field

    def init_pk(cls):
        if not cls._meta.pk_field:
            cls.id = IntegerField(unique=True, primary_key=True)
            cls.init_fields({'id': cls.id})
        cls.pk = PrimaryKeyAbstractField(cls._meta.pk_field)

    def init_relations(cls, fields):
        for name, field in fields.iteritems():
            if IField.providedBy(field):
                field.set_attrname(name)
                if not cls._meta.abstract:
                    field.set_model(cls)
                if IManyToManyField.providedBy(field):
                    cls._meta.m2m_fields.append(field)
                elif IRelatedField.providedBy(field):
                    cls._meta.foreign_keys.append(field)


@implementer(IModel)
class Model(object):
    __metaclass__ = ModelMetaclass
    triggers = NotImplemented
    manager = Manager()

    class Meta:
        db_table = ''
        ordering = []

    def __init__(self, **kwargs):
        self.__lock = defer.DeferredLock()
        self._changed_fields = set()
        for k, v in kwargs.iteritems():
            setattr(self, k, v)

    @classmethod
    def get_meta(cls):
        return cls._meta

    @classmethod
    def load(cls, values):
        model = cls()
        for value, field in zip(values, cls.get_meta().iterfields()):
            model.__dict__[field.get_attrname()] = field.to_python(value)
        return model

    def set_pk(self, value):
        pk_field = self.get_meta().pk_field
        self.__dict__[pk_field.get_attrname()] = value

    def remove_pk(self, *args):
        self.set_pk(None)

    def save(self):
        return self.__lock.run(self._save)

    def _save(self):
        model = self.__class__
        if self.pk:
            self._changed_fields.update(self._meta.mutable_fields)
            changed_fields = self._changed_fields.copy()
            self._changed_fields.clear()
            d = self.triggers.before_update(self, model, changed_fields)
            d.addBoth(self._save_defer, changed_fields)
            d.addCallback(self.triggers.after_update, model,  changed_fields)
        else:
            d = self.triggers.before_insert(self, model)
            d.addBoth(self._save_defer)
            d.addCallback(self.triggers.after_insert, model)
        d.addCallback(lambda res: self)
        return d

    def _save_defer(self, res, changed_fields=()):
        if self.pk and not changed_fields:
            d = defer.succeed(self)
        elif self.pk:
            kwargs = {}
            for field_name in changed_fields:
                kwargs[field_name] = getattr(self, field_name)
            d = self.manager.update(**kwargs).filter(pk=self.pk).execute()
        else:
            d = self.manager.insert(self).execute()
        if self.get_meta().m2m_fields:
            d.addCallback(lambda res: self._save_m2m())
        d.addCallback(lambda res: self)
        return d

    def _save_m2m(self):
        defer_list = []
        for f in self.get_meta().m2m_fields:
            m2m_field = getattr(self, f.get_attrname())
            defer_list.append(m2m_field.save())
        return defer.DeferredList(defer_list)

    def delete(self):
        return self.__lock.run(self._delete)

    def _delete(self):
        if self.pk:
            model = self.__class__
            d = self.triggers.before_delete(self, model)
            d.addBoth(self._delete_defer)
            d.addCallback(self.triggers.after_delete, model)
        else:
            d = defer.succeed(self)
        d.addCallback(lambda a: self)
        return d

    def _delete_defer(self, *args):
        d = self.manager.delete().filter(pk=self.pk).execute()
        d.addCallback(self.remove_pk)
        d.addCallback(lambda a: self)
        return d

    def __setattr__(self, key, value):
        object.__setattr__(self, key, value)
        if self.pk and key in self.get_meta().fields:
            self._changed_fields.add(key)

    def __new__(cls, *args, **kwargs):
        new_obj = object.__new__(cls, *args, **kwargs)
        for field in new_obj.get_meta().iterfields():
            if field.has_default():
                value = field.get_default()
            else:
                value = None
            new_obj.__dict__[field.get_attrname()] = value
        return new_obj

    def __hash__(self):
        return hash(self.pk)

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        return self.pk == other.pk

