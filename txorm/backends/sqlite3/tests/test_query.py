# -*-coding:utf-8-*-
from twisted.trial import unittest
from twisted.internet import defer
from txorm.backends.sqlite3.backend import SQLite3Backend
from txorm.router import SimpleRouter, register_router
from txorm.gis import models


class UserPermission(models.Model):
    class Meta:
        db_table = 'user_permissions'

    name = models.CharField(max_length=30)


class AbstractUser(models.Model):
    class Meta:
        abstract = True

    username = models.CharField(max_length=30)
    first_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=30, blank=True)
    is_superuser = models.BooleanField(default=False)


class User(AbstractUser):
    class Meta:
        db_table = 'users'

    permissions = models.ManyToManyField(UserPermission, related_name='users')


class UserSession(models.Model):
    class Meta:
        db_table = 'user_sessions'

    user = models.OneToOneField('User', related_name='session')


# class Order(models.Model):
#
#     price = models.IntegerField()
#     user = models.ForeignKey(User, related_name='orders')



# q = User.manager.select().filter(username__ilike='vas')
# # q.select_related('permissions').filter(name='ddddd')
# user = yield q.execute()
# if not user.session:
#     yield user.orders.filter(price__lt=10).execute()
#
# user.orders_obj

create_models_query = """
CREATE TABLE "user_permissions" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "name" varchar(30) NOT NULL
)
;
CREATE TABLE "users_permissions" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "user_id" integer NOT NULL,
    "userpermission_id" integer NOT NULL REFERENCES "user_permissions" ("id"),
    UNIQUE ("user_id", "userpermission_id")
)
;
CREATE TABLE "users" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "username" varchar(30) NOT NULL,
    "first_name" varchar(30) NOT NULL,
    "last_name" varchar(30) NOT NULL,
    "is_superuser" bool NOT NULL
)
;
CREATE TABLE "user_sessions" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "user_id" integer NOT NULL UNIQUE REFERENCES "users" ("id")
)
;
CREATE INDEX "users_permissions_6340c63c" ON "users_permissions" ("user_id");
CREATE INDEX "users_permissions_a73244ed" ON "users_permissions" ("userpermission_id");
"""


drop_models_query = """
DROP TABLE "users_permissions";
DROP TABLE "user_permissions";
DROP TABLE "user_sessions";
DROP TABLE "users";
"""


def executescript(cursor, sql):
    cursor.executescript(sql)


class SQLiteTest(unittest.TestCase):
    db_path = '/tmp/txorm_sqlite3_test.sql'

    def setUp(self):
        self.backend = SQLite3Backend(self.db_path)
        router = SimpleRouter(self.backend)
        register_router(router)
        return self.backend.pool.runInteraction(executescript, create_models_query)

    def tearDown(self):
        return self.backend.pool.runInteraction(executescript, drop_models_query)

    @defer.inlineCallbacks
    def create_records(self):
        permission1 = yield UserPermission(name='UserPermission1').save()
        permission2 = yield UserPermission(name='UserPermission2').save()
        user = User()
        user.username = 'test_user'
        user.first_name = 'test'
        user.permissions.add(permission1, permission2)
        yield user.save()
        # user.permissions.remove(permission1)
        # yield user.save()
        # user.permissions.clear()
        yield user.save()
        user_sesssion = UserSession()
        user_sesssion.user = user
        print user_sesssion.__dict__
        print user_sesssion.user.cache
        yield user_sesssion.save()
        print '='*10
        yield user.session.manager.all()
        print user.session.cache
        yield user.permissions.manager.all()
        print '-'*10
        us = yield UserPermission.manager.filter(users__username=user.username).last()
        uss, created = yield UserSession.manager.get_or_create(user_id=user.pk)
        su = yield uss.user.manager.execute()
        print su, uss.user.cache
        print su.__dict__
        print '*' * 50
        user = User.manager.select()
        user.select_related('permissions')
        u = yield user.first()
        print u.permissions.cache
        print '*'* 50
        q = UserPermission.manager.select()
        q.select_related('users').select_related('session')
        permissions = yield q.all()
        print permissions, permissions[0].users.cache[0].session.cache
        s = yield User.manager.select('username', 'session', 'permissions__name').all()
        print s

    def test_insert(self):
        return self.create_records()

