# -*-coding:utf-8-*-
from txorm.backends.backend import AbstractBackend
from txorm.backends.sqlite3.compiler import SQLite3Compiler


class SQLite3Backend(AbstractBackend):
    db_driver = 'sqlite3'
    support_returning = False
    support_for_update = False
    compiler = SQLite3Compiler()

    def __init__(self, database, **options):
        self.database = database
        self.options = options
        self.pool = self.connection_pool_factory(self.get_db_driver_name(), self.get_connection_params())

    def get_connection_params(self):
        connection_params = self.connection_options.copy()
        connection_params.update(self.options)
        connection_params['database'] = self.database
        return connection_params

